import { ReactNode } from "react";
import NextLink from "next/link";
import {
  Box,
  Flex,
  Avatar,
  Link,
  Button,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  MenuDivider,
  useDisclosure,
  useColorModeValue,
  Stack,
  useColorMode,
  Center,
  Image,
  Heading,
} from "@chakra-ui/react";
import { MoonIcon, SunIcon } from "@chakra-ui/icons";

// const NavLink = ({ children }) => (
//   <Link
//     px={2}
//     py={1}
//     rounded={"md"}
//     _hover={{
//       textDecoration: "none",
//       bg: useColorModeValue("gray.200", "gray.700"),
//     }}
//     href={"#"}
//   >
//     {children}
//   </Link>
// );

export default function Nav() {
  const { colorMode, toggleColorMode } = useColorMode();
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <>
      <Flex
        bg={useColorModeValue("white", "gray.900")}
        px={4}
        height={"100%"}
        alignItems={"center"}
        justifyContent={"space-between"}
        boxShadow="xl"
      >
        <Box className="main-header__logo">
          <Link as={NextLink} href="/">
            <Heading as="h1" size="xl">
              <Image
                src="https://res.cloudinary.com/dzs1ujqtn/image/upload/v1674748727/logo-no-background_s7szpa.svg"
                alt="Home"
              />
            </Heading>
          </Link>
        </Box>

        <Flex alignItems={"center"} className="main-header__menu">
          <Stack direction={"row"} spacing={7}>
            <Button onClick={toggleColorMode}>
              {colorMode === "light" ? <MoonIcon /> : <SunIcon />}
            </Button>

            <Menu>
              <MenuButton
                as={Button}
                rounded={"full"}
                variant={"link"}
                cursor={"pointer"}
                minW={0}
                className='menu-trigger'
              >
                <Avatar bg="primary.500" size={'sm'}/>
              </MenuButton>
              <MenuList alignItems={"center"}>
                <br />
                <Center>
                <Avatar bg="primary.500" size={'xl'}/>
                </Center>
                <br />
                <Center>
                  <p>Username</p>
                </Center>
                <br />
                <MenuDivider />
                <MenuItem>
                  <Link as={NextLink} href="/login" w='100%'>
                    Login
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link as={NextLink} href="/profile" w='100%'>
                    Profile
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link as={NextLink} href="/about" w='100%'>
                    About
                  </Link>
                </MenuItem>
                {/* <MenuItem>Account Settings</MenuItem>
                <MenuItem>Logout</MenuItem> */}
              </MenuList>
            </Menu>
          </Stack>
        </Flex>
      </Flex>
    </>
  );
}
