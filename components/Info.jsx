import React from "react";
import { BiGasPump, BiMap, BiTime } from "react-icons/bi";
import { stringCoordsToNumber } from "../utils/stringCoordsToNumber.js";
import { Button, Flex, Heading, Link, Text, useColorModeValue } from "@chakra-ui/react";

const Info = () => {
  return (
    <>
      <Heading
        as="h3"
        size="md"
        display={"flex"}
        alignItems={"center"}
        color={"primary.500"}
      >
        <BiGasPump fontSize={32} mr={4} display={"flex"} />
        {gasolinera.Rótulo}
      </Heading>
      <Text pt="2" fontSize="sm" display={"flex"} alignItems={"flex-start"}>
        <BiMap fontSize={"20px"} mr={2} display={"flex"} />{" "}
        {gasolinera.Localidad}, {gasolinera.Dirección}, {gasolinera["C.P."]}
      </Text>
      <Flex justifyContent="space-between" my={2} alignItems={"flex-start"}>
        <Text pt="2" fontSize="sm" display={"flex"} alignItems={"center"}>
          <BiTime fontSize={"20px"} mr={4} display={"flex"} />{" "}
          {gasolinera.Horario}
        </Text>
        <Button
          colorScheme="primary"
          variant="solid"
          size="sm"
          onClick={openMarker}
        >
          <Link
            target="_blank"
            href={`https://www.google.com/maps?q=${stringCoordsToNumber(
              gasolinera.Latitud
            )},${stringCoordsToNumber(gasolinera["Longitud (WGS84)"])}`}
          >
            + Info
          </Link>
        </Button>
      </Flex>
    </>
  );
};

export default Info;
