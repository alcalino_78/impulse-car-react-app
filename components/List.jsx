import {
  Box,
  Flex,
  Heading,
  Button,
  SkeletonCircle,
  SkeletonText,
  Text,
  useColorModeValue,
  Link,
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { reverseGeoCoding } from "../src/pages/api/index";
import SupplierDetail from "./SupplierDetail";
import Loader from "./Spinner";
import { BiGasPump, BiMap, BiTime } from "react-icons/bi";

const List = ({ gasolineras, coordinates }) => {
  const bg = useColorModeValue("blackAlpha.700", "white");
  const fieldsSkeleton = []; // repeat n veces un elemento
  for (let i = 1; i <= 4; i++) {
    fieldsSkeleton.push(
      <Box
        padding="6"
        boxShadow="lg"
        bg={useColorModeValue("whiteAlpha.900", "gray.900")}
        key={i}
      >
        <SkeletonCircle size="10" />
        <SkeletonText mt="4" noOfLines={4} spacing="4" skeletonHeight="2" />
      </Box>
    );
  }

  return (
    <>
      <Flex flex={1} overflowY={"scroll"} direction={"column"} py={2}>
        {gasolineras !== null ? (
          <>
            <Heading pl={4} my={2} color={"primary.500"}>
              {gasolineras.length}{" "}
              <Text as="span" color={bg} fontSize={28}>
                gasolineras más cercanas
              </Text>
            </Heading>
            {gasolineras.map((gasolinera) => (
              <div key={gasolinera.IDEESS}>
                <SupplierDetail gasolinera={gasolinera} gasolineras={gasolineras} coordinates={coordinates}/>
                
              </div>
            ))}
          </>
        ) : (
          [fieldsSkeleton]
        )}
      </Flex>
    </>
  );
};

export default List;
