import {
  Box,
  Button,
  Divider,
  Flex,
  Grid,
  GridItem,
  Heading,
  Link,
  Stat,
  StatLabel,
  StatNumber,
  Text,
  useColorMode,
} from "@chakra-ui/react";
import React, { useEffect, useMemo, useState } from "react";
// import GoogleMapReact from 'google-map-react'
import { GoogleMap, InfoWindow, Marker } from "@react-google-maps/api";
import { stringCoordsToNumber } from "../utils/stringCoordsToNumber.js";
import { darkModeMaps } from "./../utils/stylesMap";
import { BiGasPump, BiMap, BiMapPin, BiTime } from "react-icons/bi";

const markers = [
  {
    id: 3,
    name: "Restaurante RUS",
    position: {
      lat: parseFloat("40.494739533815974"),
      lng: parseFloat("-3.3785675247830667"),
    },
  },
];

const Map = ({ coordinates, setCoordinates, gasolineras }) => {
  const [activeMarker, setActiveMarker] = useState(null);
  const { colorMode, toggleColorMode } = useColorMode();

  // const center = useMemo(() => (coordinates), [])

  const handleActiveMarker = (marker) => {
    if (marker === activeMarker) {
      return;
    }
    setActiveMarker(marker);
  };

  // const handleOnLoad = (map) => {
  //   const bounds = new google.maps.LatLngBounds();
  //   console.log("coordinates: ", coordinates);
  //   bounds.extend(coordinates);

  //   map.fitBounds(bounds);
  // };
  const handleOnLoad = (map) => {
    const bounds = new google.maps.LatLngBounds();
    markers.forEach(({ position }) => bounds.extend(position));
    map.fitBounds(bounds);
  };

  function reportPost(markerid) {
    console.log(markerid);
  }

  return (
    <Box>
      {console.log(coordinates)}
      <GoogleMap
        id="pushMyCarMap"
        // onLoad={handleOnLoad}
        onHeadingChanged={(e) => {
          setCoordinates({ lat: e.center.lat, lng: e.center.lng });
          setBounds({ ne: e.marginBounds.ne, sw: e.marginBounds.sw });
        }}
        onClick={() => setActiveMarker(null)}
        mapContainerStyle={{ width: "100%", height: "calc(100vh - 60px)" }}
        mt={"60px"}
        maxH="calc(100vh - 60px)"
        mapContainerClassName={"map-container"}
        center={coordinates}
        // center={{
        //   lat: -3.745,
        //   lng: -38.523
        // }}
        zoom={14}
        options={{
          styles: colorMode === "dark" ? darkModeMaps : "",
          // mapId: colorMode === "dark" ? "adbaafa60cae84f5" : "d424c6369514dc6e",
        }}
      >
        {console.log(coordinates)}
        {gasolineras !== null
          ? gasolineras.map((gasolinera) => (
              <Marker
                key={gasolinera.IDEESS}
                // label={gasolinera.IDEESS}
                position={{
                  lat: stringCoordsToNumber(gasolinera.Latitud),
                  lng: stringCoordsToNumber(gasolinera["Longitud (WGS84)"]),
                }}
                onClick={() => handleActiveMarker(gasolinera.IDEESS)}
                animation="drop"
                icon={
                  "https://res.cloudinary.com/dzs1ujqtn/image/upload/c_thumb,w_200,g_face/v1674879206/pin_z39fat.svg"
                }
              >
                {/* {console.log(stringCoordsToNumber(gasolinera.Latitud), stringCoordsToNumber(gasolinera["Longitud (WGS84)"]))} */}
                {activeMarker === gasolinera.IDEESS ? (
                  <InfoWindow onCloseClick={() => setActiveMarker(null)}>
                    <div>
                      <Heading
                        as="h3"
                        size="md"
                        display={"flex"}
                        alignItems={"center"}
                        color={"primary.500"}
                      >
                        <BiMap fontSize={32} mr={4} display={"flex"} />
                        {gasolinera.Rótulo}
                      </Heading>
                      <Text
                        pt="2"
                        fontSize="sm"
                        display={"flex"}
                        alignItems={"flex-start"}
                      >
                        <BiMapPin fontSize={"20px"} mr={2} display={"flex"} />{" "}
                        {gasolinera.Localidad}, {gasolinera.Dirección},{" "}
                        {gasolinera["C.P."]}
                      </Text>
                      <Flex
                        justifyContent="space-between"
                        my={2}
                        alignItems={"flex-start"}
                      >
                        <Text
                          pt="2"
                          fontSize="sm"
                          display={"flex"}
                          alignItems={"center"}
                        >
                          <BiTime fontSize={"20px"} mr={4} display={"flex"} />{" "}
                          {gasolinera.Horario}
                        </Text>
                      </Flex>
                      <Grid templateColumns='repeat(2, 1fr)' gap={6} mb={4} py={4}>
                        <GridItem w='100%' h='10'>
                          {gasolinera['Precio Gasoleo A'] !== '' ? (
                            <Stat>
                              <StatLabel display={'flex'}><BiGasPump fontSize={24} mr={4} display={"flex"} />Gasóleo A</StatLabel>
                              <StatNumber>{gasolinera['Precio Gasoleo A']}</StatNumber>
                            </Stat>
                          ) : null}
                        </GridItem>
                        <GridItem w='100%' h='10'>
                          {gasolinera['Precio Gasoleo Premium'] !== '' ? (
                            <Stat>
                              <StatLabel display={'flex'} className='color-diesel-premium'><BiGasPump fontSize={24} mr={4} display={"flex"} width={'40px'} />Gasóleo A Premium</StatLabel>
                              <StatNumber>{gasolinera['Precio Gasoleo Premium']}</StatNumber>
                            </Stat>
                          ) : null}
                        </GridItem>
                        <GridItem w='100%' h='10'>
                          {gasolinera['Precio Gasolina 95 E5'] !== '' ? (
                            <Stat>
                              <StatLabel display={'flex'} className='color-gas-95'><BiGasPump fontSize={24} mr={4} display={"flex"} />Gasolina 95</StatLabel>
                              <StatNumber>{gasolinera['Precio Gasolina 95 E5']}</StatNumber>
                            </Stat>
                          ) : null}
                        </GridItem>
                        <GridItem w='100%' h='10'>
                          {gasolinera['Precio Gasolina 98 E5'] !== '' ? (
                            <Stat>
                              <StatLabel display={'flex'} className='color-gas-98'><BiGasPump fontSize={24} mr={4} display={"flex"} />Precio Gasolina 98</StatLabel>
                              <StatNumber>{gasolinera['Precio Gasolina 98 E5']}</StatNumber>
                            </Stat>
                          ) : null}
                        </GridItem>
                      </Grid>
                      <Divider />
                      <Button colorScheme="primary" variant="solid" mt={4} width="full">
                        <Link
                          target="_blank"
                          href={`https://www.google.com/maps?q=${stringCoordsToNumber(
                            gasolinera.Latitud
                          )},${stringCoordsToNumber(
                            gasolinera["Longitud (WGS84)"]
                          )}`}
                        >
                          git Push My Car Please!!
                        </Link>
                      </Button>
                      {/* <button onClick={reportPost(gasolinera.IDEESS)}>
                        Report Post
                      </button> */}
                    </div>
                  </InfoWindow>
                ) : null}
              </Marker>
            ))
          : "No hay gasolineras para mostrar"}
      </GoogleMap>
    </Box>
  );
};

export default Map;
