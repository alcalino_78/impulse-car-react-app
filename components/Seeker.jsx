import React, { useRef } from "react";
import { StandaloneSearchBox } from "@react-google-maps/api";
import { Input } from "@chakra-ui/react";

const Seeker = ({ setCoordinates }) => {
    const inputRef = useRef();

    const handlePlaceChanged = () => { 
        const [ place ] = inputRef.current.getPlaces();
        if(place) { 
            console.log('place.adr_address: ',place.adr_address)
            console.log(place.geometry.location.lat())
            console.log(place.geometry.location.lng())
            console.log(place);
            console.log(place.address_components[0].long_name);
            console.log(place.geometry.viewport.southwest);
            const lat = place.geometry.location.lat();
            const lng = place.geometry.location.lng();
            setCoordinates({ lat, lng });
        } 
    }

    return (
      <StandaloneSearchBox
          onLoad={ref => inputRef.current = ref}
          onPlacesChanged={handlePlaceChanged}
      >
          <Input variant='outline' placeholder='Enter Location' className="form-control"/>

          {/* <input
              type="text"
              className="form-control"
              placeholder="Enter Location"
          /> */}
      </StandaloneSearchBox>
    );
};

export default Seeker;