import { Flex, Spinner, useColorModeValue } from "@chakra-ui/react";

import React from "react";

const loader = () => {
  return (
    <Flex
      className="loader"
      justifyContent={"center"}
      alignItems={"center"}
      width={"100vw"}
      h="100vh"
      maxH="calc(100vh - 120px)"
    >
      <Spinner
        bg={useColorModeValue("whiteAlpha.900", "gray.900")}
        thickness="4px"
        speed="0.65s"
        emptyColor="gray.200"
        color="blue.500"
        size="xl"
      />
    </Flex>
  );
};

export default loader;
