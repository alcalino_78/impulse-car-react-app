import {
  Button,
  Flex,
  Heading,
  Link,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import React from "react";
import { BiGasPump, BiMap, BiMapPin, BiTime } from "react-icons/bi";
import { stringCoordsToNumber } from "../utils/stringCoordsToNumber.js";
import {
  distanceInMeters,
  roundedDistance,
} from "../utils/computeDistanceBetween.js";

const SupplierDetail = ({ gasolinera, gasolineras, coordinates }) => {
  function openMarker(key) {
    google.maps.event.trigger(markers[id], "click");
    map.panTo(markers[id].getPosition());
  }
  return (
    <Flex
      bg={useColorModeValue("white", "gray.900")}
      px={4}
      py={2}
      m={2}
      mr={4}
      shadow="lg"
      direction={"column"}
      alignItems={"inherit"}
      justifyContent="space-between"
      className="list-item"
      cursor={"pointer"}
      onClick={() => openMarker(gasolinera.IDEESS)}
    >
      <Heading
        as="h3"
        size="md"
        display={"flex"}
        alignItems={"center"}
        color={"primary.500"}
      >
        <BiMap fontSize={32} mr={4} display={"flex"} />
        {gasolinera.Rótulo}
      </Heading>
      <Text pt="2" fontSize="sm" display={"flex"} alignItems={"flex-start"}>
        <BiMapPin fontSize={"20px"} mr={2} display={"flex"} />{" "}
        {gasolinera.Localidad}, {gasolinera.Dirección}, {gasolinera["C.P."]}
      </Text>
      <Text pt="2" fontSize="sm" display={"flex"} alignItems={"flex-start"}>
        <BiMapPin fontSize={"20px"} mr={2} display={"flex"} />
        {`${roundedDistance(
          distanceInMeters(
            coordinates.lat,
            coordinates.lng,
            stringCoordsToNumber(gasolinera.Latitud),
            stringCoordsToNumber(gasolinera["Longitud (WGS84)"])
          )
        )}`}
      </Text>
      <Flex justifyContent="space-between" my={2} alignItems={"flex-start"}>
        <Text pt="2" fontSize="sm" display={"flex"} alignItems={"center"}>
          <BiTime fontSize={"20px"} mr={4} display={"flex"} />{" "}
          {gasolinera.Horario}
        </Text>
        {/* <Button colorScheme="primary" variant="solid" size='sm' onClick={openMarker}>
          <Link
            target="_blank"
            href={`https://www.google.com/maps?q=${stringCoordsToNumber(
              gasolinera.Latitud
            )},${stringCoordsToNumber(gasolinera["Longitud (WGS84)"])}`}
          >
            + Info
          </Link>
        </Button> */}
      </Flex>
    </Flex>
  );
};

export default SupplierDetail;
