import { Grid, GridItem } from "@chakra-ui/react";
import Header from "./Header";


export default function Layout({ children }) {
  return (
    <>
      <Grid
        templateAreas={`"header" "main"`}
        gridTemplateRows={"60px 1fr"}
        gridTemplateColumns={"1fr"}
        h="100vh"
        gap="0"
        className="main"
        overflow={'hidden'}
      >
        <GridItem area={"header"} className="main-header"
          position={'fixed'}
          w={'100%'}
          h={'60px'}
          zIndex={'1000'}
        >
          <Header />
        </GridItem>
        <main className="cont-main">{children}</main>

        {/* <GridItem pl="2" bg="blue.300" area={"footer"}>
        Footer
      </GridItem> */}
      </Grid>
    </>
  );
}
