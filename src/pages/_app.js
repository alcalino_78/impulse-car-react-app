import { ChakraProvider } from '@chakra-ui/react'
import '../../styles/globals.css'
import Layout from './../../components/layout';
import theme from '../theme'
import Head from 'next/head';
import { AuthProvider } from '../../context/auth-context';



export default function App({ Component, pageProps }) {
  const urlGoogleApis = `https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY}&callback=Function.prototype&sensor=false`;

  return (
    <AuthProvider>
      <ChakraProvider cssVarsRoot='#app' theme={theme}>
        <Layout>
          <Head>
            <script src={urlGoogleApis}></script>
          </Head>
          <Component {...pageProps} />
        </Layout>
      </ChakraProvider>
    </AuthProvider>
  )
}