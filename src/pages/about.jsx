import { Box, Flex, Heading, Link, Stack, Text, useColorModeValue } from "@chakra-ui/react";
import { InfoIcon } from "@chakra-ui/icons";

export default function Info() {
  return (
    <Flex
      flexDirection="column"
      width="100wh"
      height="100vh"
      bg={useColorModeValue("gray.200", "gray.700")}
      justifyContent="center"
      alignItems="center"
      px={10}
    >
      <Stack
        flexDir="column"
        mb="2"
        justifyContent="center"
        alignItems="center"
      >
        <InfoIcon boxSize={"50px"} color={"blue.500"} />
        <Heading as="h2" size="xl" mt={6} mb={2}>
          About Us
        </Heading>
        <Text color={"gray.500"} textAlign='justify'>
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
          nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
          sed diam voluptua.
        </Text>
      </Stack>
    </Flex>
  );
}
