// import Geocode from "react-geocode";

// Geocode.setApiKey(process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY);

// // set response language. Defaults to english.
// Geocode.setLanguage("es");

// // set response region. Its optional.
// // A Geocoding request with region=es (Spain) will return the Spanish city.
// Geocode.setRegion("es");

// // set location_type filter . Its optional.
// // google geocoder returns more that one address for given lat/lng.
// // In some case we need one address as response for which google itself provides a location_type filter.
// // So we can easily parse the result for fetching address components
// // ROOFTOP, RANGE_INTERPOLATED, GEOMETRIC_CENTER, APPROXIMATE are the accepted values.
// // And according to the below google docs in description, ROOFTOP param returns the most accurate result.
// Geocode.setLocationType("ROOFTOP");

// export const reverseGeoCoding = (lat, lng) => {
  
//   // Get formatted address, city, state, country from latitude & longitude when
//   // Geocode.setLocationType("ROOFTOP") enabled
//   // the below parser will work for most of the countries
//   Geocode.fromLatLng(lat, lng).then(
//     (response) => {
//       // const address = response.results[0].formatted_address;
//       let city, state, country;
//       for (let i = 0; i < response.results[0].address_components.length; i++) {
//         for (let j = 0; j < response.results[0].address_components[i].types.length; j++) {
//           switch (response.results[0].address_components[i].types[j]) {
//             case "locality":
//               city = response.results[0].address_components[i].long_name;
//               break;
//             case "administrative_area_level_1":
//               state = response.results[0].address_components[i].long_name;
//               break;
//             case "country":
//               country = response.results[0].address_components[i].long_name;
//               break;
//           }
//         }
//       }
//       console.log(typeof city, city);
//       // console.log(address);
//       return city
//     },
//     (error) => {
//       console.error(error);
//     }
//   );

// }


export async function reverseGeoCoding(lat, lng) {
  var latlng = new google.maps.LatLng(lat, lng);
  // This is making the Geocode request
  var geocoder = new google.maps.Geocoder();
  let reverseResult;
  const result = await geocoder.geocode({ 'latLng': latlng }, function (results, status) {
    let key;
    if (status !== google.maps.GeocoderStatus.OK) {
        console.log(status);
    }
    // This is checking to see if the Geoeode Status is OK before proceeding
    if (status == google.maps.GeocoderStatus.OK) {
      console.log(results);
      var address = (results[3].formatted_address);
      // return address;
      var city, state, country;
      for (let i = 0; i < results[3].address_components.length; i++) {
        for (let j = 0; j < results[3].address_components[i].types.length; j++) {
          switch (results[3].address_components[i].types[j]) {
            case "locality":
              city = results[3].address_components[i].long_name;
              break;
            case "administrative_area_level_1":
              state = results[3].address_components[i].long_name;
              break;
            case "country":
              country = results[3].address_components[i].long_name;
              break;
          }
        }
      }
    }
    key = {city: city, state: state, country: country};
    return results.push(key);
  });
  const resultReverseGeoCoding = result.results.slice(-1);
  return resultReverseGeoCoding;
}

// Function to get IDMunicipio
const getAllCities = async (city) => {
  try {
    const response = await fetch('https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/Listados/Municipios/');
    const data = await response.json();
    // console.log(data)
    let IDMunicipio = data.filter(object => object.Municipio === city).map(object => object.IDMunicipio)[0];
    console.log(IDMunicipio);
    return IDMunicipio
  } catch (error) {
    console.log(`Fetch Data error: ${error}`)
  }
}

const getGasolinerasData = async (setGasolineras, coordinates) => {
  try {
    // const response = await fetch('http://localhost:3000/ListaEESSPrecio');
    const municipio = await reverseGeoCoding(coordinates.lat, coordinates.lng);
    const IDMunicipio = await getAllCities(municipio[0].city);
    const response = await fetch(`https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/EstacionesTerrestres/FiltroMunicipio/${IDMunicipio}`);
    // const response = await fetch('https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/EstacionesTerrestres/FiltroMunicipio/4280');
    const data = await response.json();
    // console.log(data);
    setGasolineras(data.ListaEESSPrecio);
  } catch (error) {
    console.log(`Fetch Data error: ${error}`)
  }
}


export {
  getGasolinerasData
}