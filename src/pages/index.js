import {
  Flex,
  Grid,
  GridItem,
  Spinner,
  useColorModeValue,
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import List from "../../components/List";
import Map from "../../components/Map";
import Loader from "../../components/Spinner";
import Head from "next/head";
import { getGasolinerasData, reverseGeoCoding } from "./api";
import { BiChevronLeft, BiChevronRight } from "react-icons/bi";
import Seeker from "components/Seeker";

const Home = () => {
  const [coordinates, setCoordinates] = useState({});
  const [gasolineras, setGasolineras] = useState(null);
  const [show, setShow] = React.useState(true);
  const [isLoaded, setIsLoaded] = useState(false);

  async function runFunction() {
    navigator.geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) => {
        console.log({ latitude, longitude });
        const currentCoor = { lat: latitude, lng: longitude };
        setCoordinates(currentCoor);
        getGasolinerasData(setGasolineras, currentCoor);
      }
    );

    setIsLoaded(true); //------> how to call this one after func1 is finished doing all job
  }

  useEffect(() => {
    runFunction();
  }, []);

  // console.log(gasolineras);

  // if (!isLoaded) return <Loader />

  return (
    <>
      <GridItem area={"main"} className="main-map" position={"relative"}>
        <Flex
          className={`lists-results ${show ? "hide" : "show"}`}
          direction={"column"}
          justifyContent={"space-between"}
          h="100vh"
          maxH={{
            md: "calc(100vh - 200px)", // 48em-80em,
          }}
          width={[
            "100%", // 0-30em
            "100%", // 30em-48em
            "30vw", // 48em-62em
            "25vw", // 62em+
          ]}
          position={"absolute"}
          bg={useColorModeValue("whiteAlpha.900", "gray.900")}
          top={{
            base: "auto", // 0-48em
            md: "50%", // 48em-80em,
          }}
          transform={{
            md: "translateY(-50%)", // 48em-80em,
          }}
          zIndex={1}
          overflow="hidden"
          pl={2}
          ml={[
            "0", // 0-30em
            "0", // 30em-48em
            "10", // 48em-62em
            "10", // 62em+
          ]}
          borderRadius={{
            md: "xl", // 48em-80em,
          }}
          boxShadow="2xl"
        >
          {/* <Seeker setCoordinates={setCoordinates} /> */}
          
          <List
            gasolineras={gasolineras}
            coordinates={coordinates}            
          />
        </Flex>
        <button className="toggle-list" onClick={() => setShow(!show)}>
            {show ?  <BiChevronRight/> : <BiChevronLeft/>}
          </button>
        <Map
          setCoordinates={setCoordinates}
          coordinates={coordinates}
          gasolineras={gasolineras}
        />
      </GridItem>
    </>
  );
};

export default Home;
