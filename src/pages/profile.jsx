import {
  Avatar,
  Box,
  Button,
  Center,
  Divider,
  Flex,
  Heading,
  Link,
  Stack,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import { InfoIcon } from "@chakra-ui/icons";
import React from "react";
import Router from "next/router";
import { useContext } from "react";
import { AuthContext } from "../../context/auth-context";

import fetch from "isomorphic-unfetch";
import useSWR from "swr";
import cookie from "js-cookie";

export default function Profile() {
  // const router = useRouter();
  // const authContext = useContext(AuthContext);
  // React.useEffect(() => {
  //   // checks if the user is authenticated
  //   authContext.isUserAuthenticated()
  //   ? router.push("/profile")
  //   : router.push("/login");
  // }, []);
  const { data, revalidate } = useSWR("/api/auth/me", async function (args) {
    const res = await fetch(args);
    console.log(res);
    return res.json();
  });
  if (!data) return <h1>Loading...</h1>;
  let loggedIn = false;
  if (data.email) {
    loggedIn = true;
  }
  return (
    <Flex
      flexDirection="column"
      width="100wh"
      height="100vh"
      bg={useColorModeValue("gray.200", "gray.700")}
      justifyContent="center"
      alignItems="center"
      px={10}
    >
      <Stack
        flexDir="column"
        mb="2"
        justifyContent="center"
        alignItems="center"
      >
        {loggedIn && (
          <>
            <Avatar bg="primary.500" size={"sm"} />
            <Heading as="h2" size="xl" mt={6} mb={2}>
              User Profile
            </Heading>
            <Text color={"gray.500"} textAlign="justify" >
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua.
            </Text>
            <Flex mt={'48px'}>
              <Button 
                colorScheme="primary" 
                variant="outline"
                onClick={() => {
                  cookie.remove("token");
                  Router.push("/login");
                  // revalidate();
                }}
                className={'super-margin-top'}
              >
                  Log Out
                </Button>

            </Flex>
            
          </>
        )}
        {!loggedIn && (
          <>
            <Flex alignItems={'center'}>
              <Button colorScheme="primary" variant="solid">
                <Link href="/login">Login</Link>
              </Button>
              <Center height="70px" mx={9}>
                <Divider orientation="vertical" className="divider"/>
              </Center>
              <Button colorScheme="primary" variant="outline">
                <Link href="/signup">Sign Up</Link>
              </Button>
            </Flex>
          </>
        )}
      </Stack>
    </Flex>
  );
}
