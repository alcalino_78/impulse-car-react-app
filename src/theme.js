import { extendTheme } from '@chakra-ui/react'

const theme = extendTheme({
  colors: {
    primary: {
      main: "#02b6ff",
      50: "#e3f2fd",
      100: "#bbdefb",
      200: "#90caf9",
      300: "#64b5f6",
      400: "#42a5f5",
      500: "#02b6ff",
      600: "#1e88e5",
      700: "#1976d2",
      800: "#1565c0",
      900: "#0d47a1"
    }
  },
  components: {
    Button: {
      // 1. We can update the base styles
      // baseStyle: {
      //   fontWeight: 'bold', // Normally, it is "semibold"
      // },
      // // 2. We can add a new button size or extend existing
      // sizes: {
      //   xl: {
      //     h: '56px',
      //     fontSize: 'lg',
      //     px: '32px',
      //   },
      // },
      // // 3. We can add a new visual variant
      // variants: {
      //   'with-shadow': {
      //     bg: 'red.400',
      //     boxShadow: '0 0 2px 2px #efdfde',
      //   },
      //   // 4. We can override existing variants
      //   solid: (props: StyleFunctionProps) => ({
      //     bg: props.colorMode === 'dark' ? 'red.300' : 'red.500',
      //   }),
      //   // 5. We can add responsive variants
      //   sm: {
      //     bg: 'teal.500',
      //     fontSize: 'md',
      //   },
      // },
      // 6. We can overwrite defaultProps
      defaultProps: {
        size: 'lg', // default is md
        variant: 'sm', // default is solid
        colorScheme: 'primary', // default is gray
      }
    },
  }
});

export default theme;