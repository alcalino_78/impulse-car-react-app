const distanceInMeters = (paramlat1, paramlng1, paramlat2, paramlng2) =>
  google.maps.geometry.spherical.computeDistanceBetween(
    new google.maps.LatLng({
      lat: paramlat1,
      lng: paramlng1,
    }),
    new google.maps.LatLng({
      lat: paramlat2,
      lng: paramlng2,
    })
  );

const roundedDistance = (distance) => {
  if (distance >= 1000) {
    return  (distance * 0.001).toFixed(3).replace(".", ",") + " km." ;
  } else {
    return Math.trunc(distance) + " m." ;
  }
};

export { distanceInMeters, roundedDistance };
